import axios from 'axios';

const url = "http://localhost:5000/users";

export const getAllPosts = () => axios.get(url);
export const createPost = (data) => axios.post(url, data);
export const deletePost = (id) => axios.delete(`${url}/${id}`);