import { getAllPosts, createPost, deletePost } from "../api/index";

// posts reducer's actions

// here we need to fetch all posts, so in redux we use thunk for async functions
// this is the syntax ..., add another async and dispatch action instead of return.
export const getPostsAction = () => async (dispatch) => {
    try {
        const { data } = await getAllPosts();
        const action = {
            type: 'GET_ALL',
            payload: data,
        }
        dispatch(action);
    } catch (error) {
        console.log(error.message);
    }
}

export const createPostAction = (newPostData) => async (dispatch) => {
    try {
        const { data } = await createPost(newPostData);
        const action = {
            type: 'CREATE',
            payload: data,
        }
        dispatch(action);
    } catch (error) {
        console.log(error.message);
    }
}

export const deletePostAction = (postId) => async (dispatch) => {
    try {
        await deletePost(postId);
        const action = {
            type: 'DELETE',
            payload: postId,
        }
        dispatch(action);
    } catch (error) {
        console.log(error.message);
    }
}