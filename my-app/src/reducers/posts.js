const postsReducer = (posts=[], action) => {
    switch (action.type) {
        case 'GET_ALL':
            return action.payload;
        case 'CREATE':
            return [...posts, action.payload];
        case 'DELETE':
            return posts.filter(post => post._id !== action.payload);
        default: return posts;
    }
}

export default postsReducer;