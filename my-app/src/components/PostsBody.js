import React, { useEffect } from "react";
import { useDispatch, useSelector } from 'react-redux';

import Post from "./Post";
import { getPostsAction } from '../actions/posts';

const PostsBody = () => {

    const dispatch = useDispatch();

    // fetching posts
    useEffect(() => {
        dispatch(getPostsAction());
    }, [dispatch]);

    const posts = useSelector((globalState) => globalState.postsReducer);
    console.log(posts);

    return (
            <div className="content__postsBody">
                {!posts.length ? <p>No posts yet</p> : (
                    posts.map((post) => (
                        <Post key={post._id} id={post._id} author={post.author} message={post.message} imgFile={post.imageFile} time={post.createdAt}/>
                    ))
                )}
            </div>
    );
}

export default PostsBody;
