import React from "react";
import PostsBody from "./PostsBody";
import FormBody from "./FormBody";

const Content = () => {
    return (
            <div className="content">
                <PostsBody />
                <FormBody />
            </div>
    );
}

export default Content;