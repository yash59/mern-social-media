import React from "react";
import Input from '@material-ui/core/Input';
import Avatar from '@material-ui/core/Avatar';
import HomeIcon from '@material-ui/icons/Home';
import SearchIcon from '@material-ui/icons/Search';
import TextsmsIcon from '@material-ui/icons/Textsms';
import IconButton from '@material-ui/core/IconButton';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';

const Navbar = () => {
    return (
        <nav className="navbar">
            <div className="navbar__name">
                <span className="navbar__name__heading">
                    <h1>SnapShot</h1>
                </span>
            </div>
            <div className="navbar__searchBox">
                <SearchIcon></SearchIcon>
                <Input placeholder="search here" />
            </div>
            <div className="navbar__icons">
                <IconButton>
                    <HomeIcon></HomeIcon>
                </IconButton>
                <IconButton>
                    <TextsmsIcon></TextsmsIcon>
                </IconButton>
                <IconButton>
                    <FavoriteBorderIcon></FavoriteBorderIcon>
                </IconButton>
                <IconButton>
                    <Avatar />
                </IconButton>
            </div>
        </nav>
    );
}

export default Navbar;