import React, { useState } from "react";
import FileBase from 'react-file-base64';
import { TextField, Button } from '@material-ui/core';
import { useDispatch } from "react-redux";

import { createPostAction } from '../actions/posts';

const FormBody = () => {

    const dispatch = useDispatch();
    const [postData, setPostData] = useState({
        author: "",
        message: "",
        imageFile: ""
    })

    const clear = () => {
        setPostData({
            author: "",
            message: "",
            imageFile: ""
        })
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        dispatch(createPostAction(postData));
        clear();
    }

    const handleChange = (event) => {
        const value = event.target.value;
        const name = event.target.name;
        setPostData((preVal) => {
            return {
                ...preVal,
                [name]: value
            }
        })
    }

    return (
        <postsProvider>
            <div className="content__formBody">
                <form autoComplete="off" onSubmit={handleSubmit}>
                    <h2>Create A SnapShot</h2>

                    <TextField
                    className="givePadding"
                    name="author"
                    variant="outlined"
                    label="Author"
                    fullWidth
                    value={postData.author}
                    onChange={handleChange}
                    ></TextField>

                    <TextField
                    className="givePadding"
                    name="message"
                    variant="outlined"
                    label="Message"
                    fullWidth
                    value={postData.message}
                    onChange={handleChange}
                    ></TextField>

                    <div className="content__formBody__fileInput">
                        <FileBase type="file" multiple={false} onDone={
                            ({ base64 }) => setPostData({ ...postData, imageFile: base64 })
                        } />
                    </div>

                    <Button
                    className="givePadding"
                    type="submit"
                    variant="contained" 
                    color="primary" 
                    size="large"
                    fullWidth
                    >Upload !</Button>
                </form>
            </div>
        </postsProvider>
    );
}

export default FormBody;