import React from 'react';
import { useDispatch } from "react-redux";
import Avatar from '@material-ui/core/Avatar';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import ThumbUpAltIcon from '@material-ui/icons/ThumbUpAlt';
import moment from "moment";
import { deletePostAction } from '../actions/posts';

const Post = (props) => {

    const dispatch = useDispatch();

    return (
        <div className="content__postsBody__Post">
            <div className="content__postsBody__Post__author">
                <div className="author__left">
                    <Avatar />
                    <div className="author__left__userInfo">
                        <span>{props.author}</span>
                        <p>{moment(props.time).fromNow()}</p>
                    </div>
                </div>
                <div className="author__right">
                    <IconButton>
                        <MoreHorizIcon />
                    </IconButton>
                </div>
            </div>
            <div className="content__postsBody__Post__img">
                <img src={props.imgFile} alt="#"/>
            </div>
            <div className="content__postsBody__Post__msg">
                <p>{props.message}</p>
            </div>
            <div className="content__postsBody__Post__icons">
                <div className="Post__like">
                    <IconButton>
                        <ThumbUpAltIcon />
                    </IconButton>
                    <span>Like</span>
                </div>
                <div className="Post__delete">
                    <IconButton onClick={() => {dispatch(deletePostAction(props.id))}}>
                        <DeleteIcon />
                    </IconButton>
                    <span>Delete</span>
                </div>
            </div>
        </div>
    );
}

export default Post;