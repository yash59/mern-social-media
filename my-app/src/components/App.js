import React from "react";
import Navbar from "./Navbar";
import Content from "./Content";

const App = () => {
    return (
        <div className="app">
            <Navbar />
            <Content />
        </div>
    );
}

export default App;