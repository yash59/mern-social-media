import express from "express";
import mongoose from "mongoose";
import cors from "cors";
import "dotenv/config.js";

import usersRoutes from './routes/users.js';

// express app creation
const app = express();

// middlewares
app.use(express.json({
    limit: '50mb'
}));
app.use(cors());

// routes middlewares
app.use("/users", usersRoutes);

// connecting to mongoDb using mongoose
// for offline version URL = "mongodb://127.0.0.1:27017/db_name";
const URL = process.env.DB_CONNECTION;
const PORT = process.env.PORT || 5000;

mongoose.connect(URL, {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true
    })
    .then(() => console.log("Connected to DB."))
    .catch((error) => console.log(error));

app.listen(PORT, () => console.log(`Server running at PORT: ${PORT}`));