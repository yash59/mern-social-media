import User from '../models/user.js';

// users controllers

// create a post
export const createPost = async (req, res) => {
    const receivedData = req.body;
    try {
        const userData = new User({
            author: receivedData.author,
            message: receivedData.message,
            imageFile: receivedData.imageFile,
        });
        const result = await userData.save();
        res.status(201).json(result);
    } catch (error) {
        res.send(error.message);
    }
}

// get all posts
export const getAllPosts = async (req, res) => {
    try {
        const allPosts = await User.find().sort({"createdAt": "desc"});
        res.status(200).json(allPosts);
    } catch (error) {
        res.status(404).json({message: error.message});
    }
}

// delete a post with given id
export const deletePost = async (req, res) => {
    const receivedId = req.params.id;
    try {
        const result = await User.findByIdAndDelete({_id: receivedId});
        res.status(200).json(result);
    } catch (error) {
        res.status(204).json({message: error.message});
    }
}