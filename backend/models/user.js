import mongoose from "mongoose";

// document schema  creation
const userScehma = new mongoose.Schema({
    author: String,
    message: String,
    imageFile: String,
    likeCount: {
        type: Number,
        default: 0
    },
    createdAt: {
        type: Date,
        default: new Date()
    }
});

// creating users collection using model
export default new mongoose.model("User", userScehma);