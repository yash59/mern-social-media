import express from 'express';

import { createPost, getAllPosts, deletePost } from '../controllers/users.js';

const router = express.Router();

// users routes

// create post
router.post("/", createPost)

// get all posts
router.get("/", getAllPosts)

// delete a post
router.delete("/:id", deletePost)

export default router;